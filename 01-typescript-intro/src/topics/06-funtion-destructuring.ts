export interface Product {
    description: string;
    price: number;
}

// const phone: Product = {
//     description: 'POCO',
//     price: 150.0
// }

// const laptop: Product = {
//     description: 'ASUS TUF',
//     price: 1150.0
// }

interface TaxCalculationOptions {
    tax: number;
    products: Product[]
}

export function taxCalc(options: TaxCalculationOptions): number[] {
    const {tax, products} = options;
    let total = 0;

    products.forEach(({price}) => total += price);

    return [total, total*tax];
}

// const shoppingCar = [phone, laptop];
// const tax = 0.15;

//* Se peude hacer de esta manera
// const options: TaxCalculationOptions = {
//     tax,
//     products: shoppingCar
// }

// const result = taxCalc(options);

//* o de esta manera:
//* si el nombre de la variable es el mismo que el de la propiedad en la iunterface se puede obiar poniendo solo uno como por ejemplo "tax"
// const [total, impuestos]: number[] = taxCalc({tax, products: shoppingCar});

// console.log('Total:', total)
// console.log('Tax:', impuestos)



