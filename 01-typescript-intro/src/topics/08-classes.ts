export class Person {

    public name: string;
    public address: string;

    constructor() {
        this.name = 'Pavel';
        this.address = 'Oaxaca'
    }

}

const ironman = new Person();

console.log(ironman.address);