const skills: string[] =  ['Bash', 'Counter', 'Healing'];


interface Character {
    name : string;
    hp : number;
    skills : string[];
    hometown?: string;
}


const strider: Character = {
    name: 'Pavel',
    hp: 100,
    skills: ['Bash', 'Counter']
};


strider.hometown = 'Oaxaca'

console.table(strider);





export {};