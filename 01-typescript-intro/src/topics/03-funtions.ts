// function addNumbers(a: number, b: number) {
//     return a+b;
// }

// const addNumbersArrow = (a: number, b: number): string => {
//     return `${a+b}`;
// }

// function multiply(n1: number, n2?: number, base: number = 2) {
//     return n1 * base;
// }

// const result: number = addNumbers(1,2)

// const resultArrow: string = addNumbersArrow(3,4);

// const multiplyResult: number = multiply(5); 

//console.log({result, resultArrow, multiplyResult});

interface Character {
    name: string;
    hp: number;
    showHp: () => void;
}

const healCharacter = (character: Character, amount: number) => {

    character.hp += amount;

}

const pavel: Character = {
    name: "Pavel",
    hp: 50,
    showHp() {
        console.log(`Puntos de vida: ${this.hp}`);
    },
}

healCharacter(pavel, 50);
healCharacter(pavel, 20);

pavel.showHp();







export {};