// interface AudioPlayer {
//     audioVolume: number;
//     songDuration: number;
//     song: string;
//     details: Details
// }

// interface Details {
//     author: string;
//     year: number;
// }

// const audioPlayer: AudioPlayer = {
//     audioVolume: 90,
//     songDuration: 36,
//     song: "Mess",
//     details: {
//         author: "Messi",
//         year: 2011
//     }
// }

//* Los objetos anidados se pueden desestructurar de esta manera:
// const {song, details} = audioPlayer;
// const {year} = details;
//* o de esta manera:
// const {song, songDuration:duracion, details:{year}} = audioPlayer;
// //* y tambien se puede renombrar la propiedad con dos puntos adelante de la propiedad y el nombre que se le dara



// console.log('Song:', song + ', Duracion:', duracion + ', Year:', year);


const [ , , trunks = 'Not Found']: string[] = ['Goku', 'Vegueta'];

console.log('Personaje', trunks);













export {};