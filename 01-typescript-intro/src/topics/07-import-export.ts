import { Product, taxCalc } from './06-funtion-destructuring'

const shoppingCar: Product[] = [
    {
        description: 'Nokia',
        price: 100
    },
    {
        description: 'iPad',
        price: 150
    },
];


// Tax = 0.15%
const [total, impuestos]: number[] = taxCalc({tax: 0.15, products: shoppingCar});
console.log('Total:', total)
console.log('Tax:', impuestos)